import Vue from "vue";
import Vuex from "vuex";
import ApiService from "@/services/api-service";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentUser: null,
    users: [],
    totalCount: 0
  },
  getters: {
    currentUser(state) {
      return state.user;
    },
    users(state) {
      return state.users;
    },
    totalCount(state) {
      return state.totalCount;
    }
  },
  mutations: {
    SET_USERS(state, payload) {
      state.users = payload.data.map(user => {
        user.todos = [];
        return user;
      });
      state.totalCount = payload.totalCount;
    },
    SELECT_USER(state, payload) {
      state.currentUser = state.users.find(user => user.id === payload)
    },
    SET_TODOS(state, payload) {
      const index = state.users.findIndex(user => user.id === payload.id);
      state.users[index].todos = payload.data;
      state.currentUser = state.users[index];
    }
  },
  actions: {
    selectUser(store, payload) {
      store.commit("SELECT_USER", payload);
    },
    loadUsers(store, payload) {
      ApiService.fetchUsers(payload).then(responce => {
        store.commit("SET_USERS", responce);
      });
    },
    loadUser(store, payload) {
      ApiService.fetchUsers(payload).then(responce => {
        store.commit("SELECT_USER", responce.data);
      });
    },
    loadTodos(store, payload) {
      ApiService.fetchTodos(payload).then(responce => {
        store.commit("SET_TODOS", { id: payload.userId, data: responce });
      });
    }
  }
});
