import api from "@/connectors/request";

export default class ApiService {
  static async fetchUsers(props) {
    try {
      const result = props.id
        ? await api.get(`/users/${props.id}`)
        : await api.get("/users", { params: props });

      return {
        data: result.data,
        totalCount: result.headers["x-total-count"]
      };
    } catch (e) {
      throw Error(e);
    }
  }
  static async fetchTodos(props) {
    try {
      const result = await api.get("/todos", { params: props });
      return result.data;
    } catch (e) {
      throw Error(e);
    }
  }
}
