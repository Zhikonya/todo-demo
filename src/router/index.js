import Vue from "vue";
import VueRouter from "vue-router";
import UserListView from "../views/UserListView.vue";
// import TodoListView from "../views/TodoListView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: UserListView
  },
  {
    path: "/about",
    name: "About",
    exact: true,
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue")
  },
  {
    path: "/:id",
    name: "Todo",
    props: true,
    component: () =>
      import(/* webpackChunkName: "todoList" */ "../views/TodoListView.vue")
    // component: TodoListView
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
