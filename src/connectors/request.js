import axios from "axios";

const request = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com/",
  headers: {
    Accept: "application/json",
    "Content-type": "application/json",
    "X-Requested-With": "XMLHttpRequest"
  }
});

request.interceptors.response.use(
  response => response,
  error => {
    if (error.response) {
      console.log("Error response: (data, status, headers)");
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log("Error", error.message);
    }
    console.log("Request config:");
    console.log(error.config);
    return Promise.reject(error);
  }
);

export default request;
